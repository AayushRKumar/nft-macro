import time
import threading
from pynput.mouse import Button, Controller as MouseController
from pynput.keyboard import Key, KeyCode, Controller as KeyboardController
from random import *
import clipboard
from decimal import Decimal
from random import *
mouse = MouseController()
keyboard = KeyboardController()

def clickAndCopy():
        mouse.click(Button.left)
        time.sleep(0.1)
        mouse.click(Button.left)
        time.sleep(0.1)
        mouse.click(Button.left)
        time.sleep(0.05)
        keyboard.press(Key.ctrl)
        keyboard.press('c')
        time.sleep(0.1)
        keyboard.release(Key.ctrl)
        keyboard.release('c')
     
def clickSheetsTab():
        mouse.position = (123, 19)
        mouse.click(Button.left)
def clickDrawingTab():
        mouse.position = (410, 14)
        mouse.click(Button.left)
def clickOpenSeaTab():
        mouse.position = (592, 14)
        mouse.click(Button.left)
def writeOutStr(string):
    for value in string:
        time.sleep(0.05)
       
        keyboard.press(value)
        time.sleep(0.05)
        keyboard.release(value)
def typeShiftedKey(unShiftedKey):
        keyboard.press(Key.shift)
        keyboard.press(unShiftedKey)
        time.sleep(0.1)
        keyboard.release(unShiftedKey)
        keyboard.release(Key.shift)

def setFontSize(text):
  
    if len(text) <= 6:
        mouse.position = (370, 162)
        mouse.click(Button.left)
    elif len(text) <= 10:
        mouse.position = (356, 162)
       
        mouse.click(Button.left)
    else:
         mouse.position = (335, 162)
         mouse.click(Button.left)
def clearCanvas():
        mouse.position = (87, 124)
        time.sleep(.1)
        mouse.click(Button.left)
        time.sleep(.1)
        mouse.position = (99,214)
        time.sleep(.1)
        mouse.click(Button.left)
        time.sleep(.1)
        mouse.position = (1372, 162)
        time.sleep(.3)
        mouse.press(Button.left)
        time.sleep(0.1)
        mouse.release(Button.left)
        time.sleep(.3)
def click(x, y):
    mouse.position = (x, y)
    time.sleep(0.05)
    mouse.click(Button.left)  
startingPos = (168, 452 - 20)


for i in range(5):
    clickSheetsTab()
   
    startingPos = (158, startingPos[1] + (20))
    print(startingPos)
    mouse.position = startingPos
    clickAndCopy()
    
    elementName = clipboard.paste()
    mouse.position = startingPos
    mouse.move(620, 0)
    clickAndCopy()
    time.sleep(0.05)
    abundanceText = clipboard.paste()
    abundanceText = abundanceText.split(' ')[1]
    abundanceText = abundanceText[1:len(abundanceText)-2]
    clickDrawingTab()
    


    #Set bg color
    mouse.position = (45, 339)
    mouse.click(Button.left)
    mouse.position = (randint(2354, 2528), randint(694, 747))
    time.sleep(.11)
    mouse.click(Button.left)

    time.sleep(.1)
    mouse.position = (800, 594)
    time.sleep(0.1)
    mouse.click(Button.left)

  
    time.sleep(0.5)
   
    #Start writing
    mouse.position = (2353, 685)
    mouse.click(Button.left)




    time.sleep(0.1)
    mouse.position = (32, 446)
    mouse.click(Button.left)
    setFontSize(elementName)
    mouse.position = (750, 594)
    time.sleep(0.1)
    mouse.click(Button.left)
    writeOutStr(elementName)
    keyboard.press(Key.enter)
    setFontSize(abundanceText + "123")
    if len(abundanceText) <= 17:
        typeShiftedKey('9')
        writeOutStr(abundanceText)
        typeShiftedKey('5')
        typeShiftedKey('0')
    else:
        typeShiftedKey('9')
        writeOutStr(abundanceText[:17])
        keyboard.press(Key.enter)
        writeOutStr(abundanceText[17:])
        typeShiftedKey('5')
        typeShiftedKey('0')
    time.sleep(.1)
   
    #Save Image
    time.sleep(.1)
    mouse.position = (487, 117)
    time.sleep(0.1)
    time.sleep(.1)
    mouse.click(Button.left)
  
    mouse.position = (1076, 517)
    time.sleep(0.5)
    mouse.click(Button.left)
 
    time.sleep(0.1)
    mouse.position = (1861, 222)
    time.sleep(0.1)
    mouse.click(Button.left)
    time.sleep(0.1)
    
    clearCanvas()
    continue
    clickOpenSeaTab()
    click(394, 647)
  
    mouse.scroll(0, 100)
  
 
    
    mouse.position = (914, 794)
    time.sleep(0.05)
    mouse.click(Button.left)
    keyboard.press(Key.shift)
    keyboard.press(elementName[0])
    keyboard.release(elementName[0])
    keyboard.release(Key.shift)
    writeOutStr(elementName[1:])
    click(917, 1062)
    writeOutStr(("The element " + elementName +" makes up " + abundanceText + "% of the crust."))
    mouse.scroll(0, -100)
    time.sleep(0.05)
   
    #Add level
    click(1618, 372)
    time.sleep(1)
    click(1125, 472)
    writeOutStr("Abundance")
    click(1358, 472)
    if Decimal(abundanceText) >= Decimal(4.15):
        writeOutStr("Top 5")
    elif Decimal(abundanceText) > .1:
        writeOutStr(">.1%")
    elif Decimal(abundanceText) > .01:
        writeOutStr(">.01%")
    elif Decimal(abundanceText) > .001:
        writeOutStr(">.001%")
    elif Decimal(abundanceText) > .0001:
        writeOutStr(">.0001%")
    else:
        writeOutStr("<.0001%")
    click(1276, 644)
    time.sleep(0.5)
    mouse.scroll(0, 100)
    mouse.position = (115, 1361)
    time.sleep(1)
    mouse.press(Button.left)
    time.sleep(1)
    mouse.position = (988, 556)
    print("End")
    answer = input("Left Click and Add Item if it looks good. Press any key to continue")
    click(1517, 266)
    time.sleep(.3)
    mouse.position = (2430, 141)
    time.sleep(.5)
    mouse.move(0, 243)
    mouse.click(Button.left)
    time.sleep(.5)
    click(821, 622)
    time.sleep(2)
    click(2462, 204)
    time.sleep(1.5)

    
    
    
